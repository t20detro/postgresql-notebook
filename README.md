

# Image Docker Jupyter Notebook + posgresql

## build de l'image

``` bash
git clone
cd 
docker build -t postgresql-notebook .
```

## Lancement du notebook

Préparation du dossier pour la persistance des donnees
``` bash
cd 
mkdir postgresql-notebook_datas
chmod 777 postgresql-notebook_datas
```

Lancement du notebook
``` bash
docker  run --rm -p 8888:8888 -v ${PWD}:/home/jovyan/work postgresql-notebook
``` 
Le lien de connexion apparait sur la console, au format : `http://127.0.0.1:8888/?token=30cfab070e79cdfe0381efb9138d113fd57506927bdd6ca2`

Pour arrêter le notebook, <ctrl><c>. L'option '--rm' supprime automatiquement le container 


## Ménage des données une fois le TP terminé

``` bash
cd
sudo rm -rf postgresql-notebook_datas
```

## TODO

Mieux gérer les droits, pour éviter que les données soient créées avec un ID interne à l'image, imposant le `chmod 777` à la création, et le `sudo rm` à la suppression.

