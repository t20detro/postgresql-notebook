#!/bin/bash

if [ -z "$(psql --username=postgres -tAc "SELECT 1 FROM pg_roles WHERE rolname='user_tp'")" ]; then

    # Création de l'utilisateur user_tp, le mot de passe n'est pas très utile...
    psql --username=postgres -c "create role user_tp with login password 'abc' nosuperuser inherit nocreatedb nocreaterole noreplication;"

    # Création de la base ue_bd_ihm_db attribuée à l'utilisateur user_tp
    psql --username=postgres -c "create database ue_bd_ihm_db with owner=user_tp;"

    # Exécution du script SQL de création des tables
    psql -U user_tp -d ue_bd_ihm_db -f /srv/install_database_soins.sql

fi

