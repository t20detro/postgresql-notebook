#!/bin/bash
# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.
set -e

# If the run command is the default, do some initialization first
#if [ "$(which "$1")" = "/usr/local/bin/start-singleuser.sh" ]; then
if [ "$(which "$1")" = "/usr/local/bin/start-notebook.sh" ]; then
#  # Clone sample notebooks to user's notebook directory.  Assume $NB_USER's work
#  # directory if notebook directory not explicitly set.  `git clone` will fail
#  # if target directory already exists and is not empty, which likely means
#  # that we've already done it, so just ignore.
#  : ${NOTEBOOK_DIR:=/home/$NB_USER/work}
#  git clone https://gist.github.com/parente/facb555dfbae28e817e0 \
#    --depth 1 \
#    "$NOTEBOOK_DIR/notebook_count" || true

    export PGDATA=$HOME/work/.postgreSQL_datas
    export PATH=/usr/lib/postgresql/10/bin/:$PATH

    # reinitialisation de la base
    if [ -f "$HOME/work/resetdb" ];then
        rm -rf $PGDATA
	rm $HOME/work/resetdb
    fi

    # Création du répertoire des données de PostgreSQL
    if [ ! -d "$PGDATA" ];then
        mkdir -p $PGDATA
    fi

    if [ -z "$(ls -A "$PGDATA")" ]; then
        ## Initialisation
        # gosu postgres initdb
        initdb --username=postgres
    fi

    # Démarrage du serveur PostgreSQL
    pg_ctl start

    # initialisation 
    source /srv/initialisation.sh

fi

# Run the command provided
exec "$@"

